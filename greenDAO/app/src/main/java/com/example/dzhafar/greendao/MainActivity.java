package com.example.dzhafar.greendao;

import android.app.DownloadManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.greenrobot.greendao.query.Query;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final private String TAG = "MY_GREENDAO";

    private ArticleDao articleDao;
    private EditText editTextName;
    private EditText editTextBody;
    private EditText editTextID;
    private Query<Article> articleQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = (EditText) findViewById(R.id.articleName);
        editTextBody = (EditText) findViewById(R.id.acticleBody);
        editTextID = (EditText) findViewById(R.id.updateArticleID);
        Button addButton = (Button) findViewById(R.id.addButton);
        Button readButton = (Button) findViewById(R.id.readButton);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        Button updateButton = (Button) findViewById(R.id.updateButton);
        addButton.setOnClickListener(this);
        readButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
        updateButton.setOnClickListener(this);

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        articleDao = daoSession.getArticleDao();

        articleQuery = articleDao.queryBuilder().orderAsc(ArticleDao.Properties.ArticleName).build();
    }

    @Override
    public void onClick(View view) {
        List<Article> articles;
        String name;
        String body;
        Date date;
        String idText;
        Long id;
        switch (view.getId()) {
            case R.id.addButton:
                name = editTextName.getText().toString();
                body = editTextBody.getText().toString();
                editTextBody.setText("");
                editTextName.setText("");
                date = new Date();
                Article article = new Article(null, name, body, date);
                articleDao.insert(article);
                break;
            case R.id.readButton:
                articles = articleQuery.list();
                Log.d(TAG,"--------------------------------");
                for (Article art: articles) {
                    Log.d(TAG, "id = " + art.getId().toString() + "; Name = " + art.getArticleName() +
                    "; Body = " + art.getArticleBody() + "; Date = " + art.getDate());
                }
                break;
            case R.id.updateButton:
                idText = editTextID.getText().toString();
                editTextID.setText("");
                id = Long.parseLong(idText);
                articles = articleDao.queryBuilder().where(ArticleDao.Properties.Id.eq(id)).orderAsc(ArticleDao.Properties.ArticleName).list();
                Log.d(TAG,"--------------------------------");
                for (Article art: articles) {
                    Log.d(TAG+" update", "id = " + art.getId().toString() + "; Name = " + art.getArticleName() +
                            "; Body = " + art.getArticleBody() + "; Date = " + art.getDate());
                    name = editTextName.getText().toString();
                    body = editTextBody.getText().toString();
                    editTextBody.setText("");
                    editTextName.setText("");
                    date = new Date();
                    art.setDate(date);
                    art.setArticleName(name);
                    art.setArticleBody(body);
                    articleDao.update(art);
                }
                break;
            case R.id.deleteButton:
                idText = editTextID.getText().toString();
                editTextID.setText("");
                id = Long.parseLong(idText);
                articles = articleDao.queryBuilder().where(ArticleDao.Properties.Id.eq(id)).orderAsc(ArticleDao.Properties.ArticleName).list();
                Log.d(TAG,"--------------------------------");
                for (Article art: articles) {
                    Log.d(TAG+" delete", "id = " + art.getId().toString() + "; Name = " + art.getArticleName() +
                            "; Body = " + art.getArticleBody() + "; Date = " + art.getDate());
                    articleDao.delete(art);
                }
                break;
        }
    }
}
