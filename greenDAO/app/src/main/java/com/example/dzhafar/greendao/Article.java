package com.example.dzhafar.greendao;

import android.util.Log;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.Date;

/**
 * Created by dzhafar on 11.12.16.
 */

@Entity(indexes = {
        @Index(value = "articleName, date DESC", unique = true)
})
class Article {
    @Id
    private Long id;

    @NotNull
    private String articleName;
    private String articleBody;
    private Date date;

    public Article() {

    }
    @Keep
    Article(Long id, @NotNull String articleName, String articleBody, Date date) {
        this.id = id;
        this.articleName = articleName;
        this.articleBody = articleBody;
        this.date = date;
    }

    Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    String getArticleName() {
        return articleName;
    }

    String getArticleBody() {
        return articleBody;
    }

    Date getDate() {
        return date;
    }

    void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    void setArticleBody(String articleBody) {
        this.articleBody = articleBody;
    }

    void setDate(Date date) {
        this.date = date;
    }
}
